import {writeAmount,readAmount, getSizeOfAmount} from '../src/Serialization'
import { expect } from 'chai'
import BigInteger = require('bigi')
import {Asset, Spend, Output, Hash, PKLock, Outpoint} from '../src/Types'
import {TxSkeleton, Input} from '../src/TxSkeleton'
import {Transaction} from '../src/Transaction'
import {Buffer} from "buffer"

describe('Amount serialization', () => {
    it('amount values less than 1E4 round trip', () => {
        for (let i = 0; i < 10000; i++) {
            const big = new BigInteger(i.toString(),undefined,undefined)

            const buffer = Buffer.alloc(getSizeOfAmount(big))

            writeAmount(big, buffer,0)
            const {amount:res} = readAmount(buffer, 0)

            expect(res.equals(big)).to.be.true
        }
    })

    it('FFFFFFFFFFFFFFFF', () => {
        const big = BigInteger.fromHex('FFFFFFFFFFFFFFFF')
        const buffer = Buffer.alloc(getSizeOfAmount(big))
        writeAmount(big, buffer,0)
        const {amount:res} = readAmount(buffer, 0)

        expect(res.equals(big)).to.be.true
    })

    it('FFFFFFFFFFFFFF00', () => {
        const big = BigInteger.fromHex('FFFFFFFFFFFFFF00')
        const buffer = Buffer.alloc(getSizeOfAmount(big))
        writeAmount(big, buffer,0)
        const {amount:res} = readAmount(buffer, 0)

        expect(res.equals(big)).to.be.true
    })

    it('0000000FFFFFFF00', () => {
        const big = BigInteger.fromHex('0000000FFFFFFF00')
        const buffer = Buffer.alloc(getSizeOfAmount(big))
        writeAmount(big, buffer,0)
        const {amount:res} = readAmount(buffer, 0)

        expect(res.equals(big)).to.be.true
    })

    it('0000000FFFF00001', () => {
        const big = BigInteger.fromHex('0000000FFFF00001')
        const buffer = Buffer.alloc(getSizeOfAmount(big))
        writeAmount(big, buffer,0)
        const {amount:res} = readAmount(buffer, 0)

        expect(res.equals(big)).to.be.true
    })

    it('9000000000', () => {
        const big = new BigInteger("9000000000", 10, undefined)

        const buffer = Buffer.alloc(getSizeOfAmount(big))
        writeAmount(big, buffer,0)
        const {amount:res} = readAmount(buffer, 0)

        expect(res.equals(big)).to.be.true
    })
})

describe('Asset serialization', () => {
    function checkAsset(name:string) {
        const asset = new Asset(name)

        const size = asset.getSize()

        const buffer = Buffer.alloc(size)

        const offset = asset.write(buffer,0)

        expect(offset).to.be.equals(size)

        const {asset:asset2,offset:offset2} = Asset.read(buffer, 0)

        expect(asset2.asset).to.be.equals(name)
        expect(offset2).to.be.equals(size)
    }

    it('serialize and deserialize Zen Asset', () => checkAsset('00'))
    it('serialize and deserialize cHash only', () => checkAsset('000000001111111111111111111111111111111111111111111111111111111111111111'))
    it('serialize and deserialize compressable Subtype', () =>
        checkAsset('000000001111111111111111111111111111111111111111111111111111111111111111' +
                                 '2222222222222222000000000000000000000000000000000000000000000000'))
    it('serialize and deserialize uncompressable Subtype', () =>
        checkAsset('000000001111111111111111111111111111111111111111111111111111111111111111' +
                                 '2222222222222222222222222222222222222222222222222222222222222200'))
})

describe('TxSkeleton serialization', () => {
    const zeroHash = new Hash('0000000000000000000000000000000000000000000000000000000000000000')
    const zen = new Asset('00')

    function bigi(value:number) {
        return BigInteger.valueOf(value)
    }

    function checkTx(txSkeleton:TxSkeleton) {
        expect(txSkeleton).to.be.deep.equals(TxSkeleton.fromHex(txSkeleton.toHex()))
    }

    it('serialize and deserialize txSkeleton with mint input', () => {
        const input = new Input({
            kind:'mint',
            spend:new Spend(zen, bigi(3))
        })

        const output = new Output(
            new PKLock(zeroHash),
            new Spend(zen, bigi(7))
        )

        checkTx(new TxSkeleton([input], [output]))
    })

    it('serialize and deserialize txSkeleton with pointedOutput input', () => {
        const input = new Input({
            kind:'pointedOutput',
            outpoint:{ kind:'outpoint',txHash:zeroHash,index:0 },
            output:new Output(
                new PKLock(zeroHash),
                new Spend(zen, bigi(9))
            )
        })

        const output = new Output(
            new PKLock(zeroHash),
            new Spend(zen, bigi(7))
        )

        checkTx(new TxSkeleton([input], [output]))
    })

    it('deserializes tx from arbitrary bytes 1', () => {
        let bs = "000000000201051d5b6b256ca3a9d3e0a809bf732d1d2878d55e3b4843171c5ddc50ffd40058020280a515de480812021d184014dc43124254ddc6b994331bc8abe5fbd6c04bc3c13020010302209b95835b9af67bdc345c0ae879b93a58d0f3f90784c982a2da52fbd71d8700a3007e00000042607417042100a515de480812021d184014dc43124254ddc6b994331bc8abe5fbd6c04bc3c130002001022002add2eb8158d81b7ea51e35ddde9c8a95e24449bdab8391f40c5517bdb9a3e180a515de480812021d184014dc43124254ddc6b994331bc8abe5fbd6c04bc3c13020010001026700a515de480812021d184014dc43124254ddc6b994331bc8abe5fbd6c04bc3c13003627579010c010d72657475726e4164647265737308022002add2eb8158d81b7ea51e35ddde9c8a95e24449bdab8391f40c5517bdb9a3e10301010102000000000000000241"
        expect(Transaction.fromHex(bs).toHex() == bs).to.be.true
    })

    it('deserializes tx from arbitrary bytes 2', () => {
        let bs = "00000000020143ce67eab4c4020f6d4021a018d34bf46e9026a58421944e63187cd034a4e9ed0002803a18eaefff885ba65d4b976dce8ec09543addb5c5e9bfb2c2524f133b486c52124090302209e8c7975e8d2d701e5f036f64dbb3ab935402885fe265822dedb4f1bfe21826c0024010421003a18eaefff885ba65d4b976dce8ec09543addb5c5e9bfb2c2524f133b486c521002409022039c9bec65b496f8c5333be7f373a239ef58a882d272bc542f7a85242d8293018803a18eaefff885ba65d4b976dce8ec09543addb5c5e9bfb2c2524f133b486c5212409000201620103d35b14b19415fdcc8d46761482283b68fb82f1756cea05f09f743d99a8e6f8a8a61c994366cdff68de4433e4caf8b6c78d5e9f297d17cebd0e465f297ce0e6236bcfc486e374cf926ebf5f8f1b48011848f3a4c6904bfb604271091d1ba2f6760267003a18eaefff885ba65d4b976dce8ec09543addb5c5e9bfb2c2524f133b486c52103627579010c010d72657475726e4164647265737308022039c9bec65b496f8c5333be7f373a239ef58a882d272bc542f7a85242d82930180301010102000000000000000241"
        expect(Transaction.fromHex(bs).toHex() == bs).to.be.true
    })
})

describe('tx serialization', () => {
    it('deserialize tx with contract witness', () => {
        let bs = "000000000301a0dbe2029af0fa8ef63433593b88fea9a82e7d39894ee909b2369e36f4236968020114ecac04a23549c0fa06d5564dbf2ab0f64229b570605bd332c166deeb0576b50101ca091fbd44254ea6d74dfb177c125be52752a0715f3019ba0f6e9f3817cab67201040220f12195878fdfb0e3787c37067db17882ea9488aa4ad09523244fa713aa11862980f24db32aa1881956646d3ccbb647df71455de10cf98b635810e8870906a56b631819070080f24db32aa1881956646d3ccbb647df71455de10cf98b635810e8870906a56b63181902208248130e44060a8b944ee2c53cb98ff5a801de699b3f82bc392cd48149260a3c001819042100f24db32aa1881956646d3ccbb647df71455de10cf98b635810e8870906a56b6300185f000201620303a687e95512753893abe31db2ac985932a8d2154e0f4f31aedd776466f0b2640ef47e255562572e9ac7bc907f7a67a737536b6c92e73af2013c80223f2484f0301c6aea00c8f024d50df3dcc24386fe4ff5ce1fa24df2e176c44f57afbeab2666026a00f24db32aa1881956646d3ccbb647df71455de10cf98b635810e8870906a56b630672656465656d010c010d72657475726e416464726573730802208248130e44060a8b944ee2c53cb98ff5a801de699b3f82bc392cd48149260a3c0301010203000000000000000341"

        expect(Transaction.fromHex(bs, true,true).toHex() == bs).to.be.true
    })
})