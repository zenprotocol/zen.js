import * as bip39 from 'bip39'

export namespace Mnemonic {

    export function generateMnemonic(words: 12 | 24 = 24) {
        switch (words) {
            case 12:
                return bip39.generateMnemonic(128)
            case 24:
                return bip39.generateMnemonic(256)
        }
    }

    export const mnemonicToSeed = bip39.mnemonicToSeed
}

export default Mnemonic