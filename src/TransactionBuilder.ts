import {Asset, Hash, Input, Output, Spend, PKLock, Chain} from './Types'
import {Transaction} from './Transaction'
import Address from './Address'
import {PrivateKey} from './Crypto'
import BigInteger = require('bigi')

interface InputWithKey {
    input:Input
    privateKey?:PrivateKey
}

export class TransactionBuilder {
    private inputs:Array<InputWithKey>
    outputs:Array<Output>

    constructor(public chain:Chain) {
        this.inputs = []
        this.outputs = []
    }

    addInput(txHash:string|Hash, index:number, privateKey?:PrivateKey) {
        if (typeof txHash === 'string') {
            txHash = new Hash(txHash)
        }

        this.inputs.push({
            privateKey:privateKey,
            input: new Input({
                    kind:'outpoint',
                    txHash:txHash,
                    index:index
                })
        })
    }

    addOutput(address:string|Hash, amount:number|BigInteger, asset:Asset|string) {
        
        let decodedAddress
        if (typeof address === 'string') {
            decodedAddress = Address.decode(this.chain, address) as Hash
        } else {
            decodedAddress = Address.decode(this.chain, address.hash) as Hash
        }

        if (typeof asset === 'string')
            asset = new Asset(asset)

        if (typeof amount === 'number')
            amount = new BigInteger(amount.toString(10),10,undefined)

        const output = new Output(new PKLock(decodedAddress),new Spend(asset,amount))
        this.outputs.push(output)
    }

    hash() {
        const inputs = this.inputs.map(input => input.input)

        const tx = new Transaction(0, inputs, this.outputs)
        return tx.hash()
    }

    getUnsignedTransaction() {
        const inputs = this.inputs.map(input => input.input)

        return new Transaction(0, inputs, this.outputs)
    }

    getKeys() {
        return this.inputs.map(input => {
            if (!input.privateKey)
                throw 'missing private key'

            return input.privateKey
        })
    }

    sign() {
        return this.getUnsignedTransaction().sign(this.getKeys())
    }
}