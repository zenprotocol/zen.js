import {reduce, has, get, set, forIn, map, isEmpty, head, isUndefined } from 'lodash'
import axios, {AxiosResponse} from 'axios'
import {ExtendedKey, PrivateKey, PublicKey} from './Crypto'
import Address from './Address'
import Data from './Data'
import {Chain, Output, PKLock, Hash, Spend, Asset, FollowingWitnesses, FeeLock, ContractWitness} from './Types'
import {TransactionBuilder} from './TransactionBuilder'
import {TxSkeleton, Input} from './TxSkeleton'
import {Transaction} from './Transaction'
import BigInteger = require('bigi')
import {sha3_256} from 'js-sha3'
import {getSizeOfString, writeString} from './Serialization'


export class Wallet {
    private unspentOutputs: Wallet.PointedOutputs
    private externalPKHash: Hash
    private changePKHash: Hash

    constructor(private zenKey: ExtendedKey, private chain: Chain, private actions: Wallet.WalletActions) {
        this.externalPKHash = zenKey.derive(0).derive(0).getPublicKey().hash()
        this.changePKHash = zenKey.derive(1).derive(0).getPublicKey().hash()
        this.unspentOutputs = []
    }

    static fromMnemonic(phrase: string, chain: Chain, actions: Wallet.WalletActions) {
        const key = ExtendedKey.fromMnemonic(phrase)
        const zenKey = key.derivePath("m/44'/258'/0'")

        return new Wallet(zenKey, chain, actions)
    }

    private getAddresses() {
        return [
            Address.getPublicKeyHashAddress(this.chain, this.externalPKHash),
            Address.getPublicKeyHashAddress(this.chain, this.changePKHash)
        ]
    }

    async refresh() {
        // TODO: we always ask for all unspent outputs, we might able to make it more efficient by requesting deltas only
        this.unspentOutputs = await this.actions.getUnspentOutputs(this.getAddresses())
    }

    getBalance() {
        return reduce(this.unspentOutputs, (balance, pointedOutput) => {
            const {asset, amount} = pointedOutput.spend

            if (has(balance, asset)) {
                const accumulatedAmount = get(balance, asset)
                return set(balance, asset, accumulatedAmount + amount)
            } else {
                return set(balance, asset, amount)
            }
        }, {})
    }

    getPublicKeyHash() {
        return this.externalPKHash
    }

    getAddress() {
        return Address.getPublicKeyHashAddress(this.chain, this.externalPKHash)
    }

    getSenderPublicKey() {
        return this.zenKey.derive(3).derive(0).getPublicKey()
    }

    async getTransactions() {
        return await this.actions.getTransactions(this.getAddresses())
    }

    private collectInputs(requiredAmounts: { [s: string]: number }) {
        const changePrivateKey = this.zenKey.derive(1).derive(0).getPrivateKey()
        const externalPrivateKey = this.zenKey.derive(0).derive(0).getPrivateKey()

        const inputs: Array<[PrivateKey, Wallet.PointedOutput]> = []
        const change: { [s: string]: number } = {}

        forIn(requiredAmounts, (amount, asset) => {
            let collectedAmount = 0

            for (let output of this.unspentOutputs) {
                // TODO: we only support spending pklock, coinbase is not yet supported
                const pkLock = output.lock.PK

                if (pkLock && output.spend.asset === asset) {
                    collectedAmount += output.spend.amount

                    let privateKey: PrivateKey

                    if (pkLock.hash == this.externalPKHash.bytes.toString('hex')) {
                        privateKey = externalPrivateKey
                    }
                    else {
                        privateKey = changePrivateKey
                    }

                    inputs.push([privateKey, output])

                    if (collectedAmount === amount) {
                        break
                    } else if (collectedAmount > amount) {
                        set(change, asset, collectedAmount - amount)
                    }
                }
            }

            if (collectedAmount < amount) {
                throw 'not enough tokens'
            }
        })

        return {inputs, change}
    }

    private getChangeAddress() {
        return Address.getPublicKeyHashAddress(this.chain, this.changePKHash)
    }

    async send(outputs: Array<Wallet.Spend>) {
        const requiredAmounts = reduce(outputs, (amounts, output) => {
            if (has(amounts, output.asset)) {
                const amount = get(amounts, output.asset)
                return set(amounts, output.asset, amount + output.amount)
            } else {
                return set(amounts, output.asset, output.amount)
            }
        }, {})

        const {inputs, change} = this.collectInputs(requiredAmounts)

        const txBuilder = new TransactionBuilder(this.chain)
        inputs.forEach(([privateKey, input]) => txBuilder.addInput(input.outpoint.txHash, input.outpoint.index, privateKey))
        outputs.forEach(output => txBuilder.addOutput(output.address, output.amount, output.asset))

        const changeAddress = this.getChangeAddress()

        forIn(change, (amount, asset) => txBuilder.addOutput(changeAddress, amount, asset))

        const tx = txBuilder.sign()

        return await this.actions.publish(tx)
    }

    async executeContract(address: string, command: string, messageBody: Data.Dictionary, provideReturnAddress: boolean, outputs?: Array<Wallet.Spend>, sign?:string) {
        const changeLock = new PKLock(this.changePKHash)
        let requiredAmounts
        let txOutputs : Array<Output> = []

        if (isEmpty(outputs)) {
            // To avoid rejection of a valid contract transaction due to possible all-mint inputs
            // or same txhash, until we implement fees, we include a temp fee of one kalapa
            let tempFeeAmount = 1
            requiredAmounts = { "00": tempFeeAmount }
            txOutputs = [ new Output(new FeeLock(), new Spend(Asset.Zen, BigInteger.valueOf(1))) ]
        }
        else {
            requiredAmounts = reduce(outputs, (amounts, output) => {
                if (has(amounts, output.asset)) {
                    const amount = get(amounts, output.asset)
                    return set(amounts, output.asset, amount + output.amount)
                } else {
                    return set(amounts, output.asset, output.amount)
                }
            }, {})
        }

        const {inputs, change} = this.collectInputs(requiredAmounts)

        const txInputs = inputs
            .map(([, input]) => {
                if (input.lock.PK !== undefined)
                    return new Input({
                        kind: 'pointedOutput',
                        outpoint: {
                            kind: 'outpoint',
                            txHash: new Hash(input.outpoint.txHash),
                            index: input.outpoint.index
                        },
                        output: new Output(new PKLock(new Hash(input.lock.PK.hash)), new Spend(new Asset(input.spend.asset), new BigInteger(input.spend.amount.toString(), 10, undefined)))
                    })
            })
            .filter((x): x is Input => !!x)

        const changeOutputs =
            map(change, (amount, asset) =>
                new Output(changeLock, new Spend(new Asset(asset), new BigInteger(amount.toString(), 10, undefined))))

        const txSkeleton = new TxSkeleton(txInputs, [ ...txOutputs,...changeOutputs ])

        if (provideReturnAddress) {
            const returnAddressDict = new Data.Dictionary([[
                "returnAddress",
                new Data.PKAddress(this.externalPKHash.bytes) ]]
            )
            if (!isEmpty(messageBody.data)) {
                messageBody = new Data.Dictionary([...messageBody.data, ...returnAddressDict.data])
            } else {
                messageBody = returnAddressDict
            }
        }

        let sender = ''
        let signKey

        if (sign) {
            signKey = this.zenKey.derivePath(sign)
            sender = signKey.getPublicKey().serialize().toString('hex')
        }

        const unsignedTx = await this.actions.executeContract(address, command, Data.serialize(messageBody), txSkeleton, sender)

        if (signKey) {
            const witness:ContractWitness =
                head(unsignedTx.witnesses.filter(w => w instanceof ContractWitness)) as ContractWitness

            const size = 32 + witness.contractId.getSize() + getSizeOfString(witness.command) + 1 +
                (isUndefined(witness.messageBody) ? 0 : witness.messageBody.getSize())

            const message = Buffer.alloc(size)
            const txHash = unsignedTx.hash()
            txHash.bytes.copy(message)
            let offset = witness.contractId.write(message, 32)
            offset = writeString(witness.command, message, offset)
            message.writeInt8(isUndefined(witness.messageBody) ? 0 : 1, offset)
            offset++

            if (!isUndefined(witness.messageBody)) {
                witness.messageBody.write(message, offset)
            }

            const messageHash = Buffer.from(sha3_256(message), 'hex')
            const signature = signKey.getPrivateKey().sign(messageHash).serialize()
            const publicKey = signKey.getPublicKey().serialize()

            witness.signature = [publicKey, signature]
        }

        const tx = unsignedTx.sign(inputs.map(([key,]) => key), FollowingWitnesses)

        return await this.actions.publish(tx)
    }
}

export namespace Wallet {
    export interface Outpoint {
        txHash: string,
        index: number
    }

    export type PointedOutput = {
        outpoint:Outpoint,
        spend: {
            asset: string,
            amount: number
        },
        lock: {
            PK?: {
                hash: string,
                address: string
            }
        }
    }

    export interface TransactionInfo {
        txHash: string,
        asset: string,
        amount: number,
        confirmations: number
    }

    export interface Spend {
        address: string,
        asset: string,
        amount: number
    }

    export type PointedOutputs = Array<PointedOutput>
    export type Transactions = Array<TransactionInfo>

    export interface WalletActions {
        getUnspentOutputs: (addresses: Array<string>) => Promise<PointedOutputs>
        getTransactions: (addresses: Array<string>) => Promise<Transactions>
        publish: (tx: Transaction) => Promise<String>
        executeContract: (address: string, command: string, messageBody: string, tx: TxSkeleton, sender:string) => Promise<Transaction>
    }

    export class RemoteNodeWalletActions implements WalletActions {
        constructor(public url: string) {

        }

        private checkResponse = (response : AxiosResponse) => {
            if (response.status === 200)
                return response.data
            else
                throw response.statusText
        }

        async getUnspentOutputs(addresses: Array<string>) {
            return axios.post<PointedOutputs>(`${this.url}/api/unspentoutputs`, addresses)
                .then(this.checkResponse)
        }

        async publish(tx: Transaction) {
            return axios.post<string>(`${this.url}/api/publishtransaction`, {tx: tx.toHex() })
                .then(this.checkResponse)
        }

        async getTransactions(addresses: Array<string>) {
            return await axios.post<Transactions>(`${this.url}/api/history`, { 'addresses': addresses, 'skip': 0, 'take': 65536 })
                .then(this.checkResponse)
        }

        async executeContract(address: string, command: string, messageBody: string, tx: TxSkeleton, sender:string) {
            let data = {
                'address': address,
                'command': command,
                'options': {
                    'sender': sender
                },
                'messageBody': messageBody,
                'tx': tx.toHex()
            }


            return await axios.post<String>(`${this.url}/api/runcontract`, data)
                .then(this.checkResponse)
                .then(hex => Transaction.fromHex(hex, true, !!sender))
        }
    }
}
