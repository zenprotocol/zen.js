import {Type,Schema, safeLoad, safeDump} from 'js-yaml'
import Address from './Address'
import {PublicKey as CryptoPublicKey, Signature as CryptoSignature} from './Crypto'
import { Chain, Hash as TypesHash } from './Types'
import BigInteger = require('bigi')
import {endsWith,replace, isInteger,toPairs,isPlainObject,sumBy} from 'lodash'
import {Buffer} from "buffer"
import {getSizeOfVarInt,writeVarInt, readVarInt} from './Serialization'

export namespace Data {
    export class DataList {
        constructor(public list:Array<String | Int64 | UInt64 | PKAddress | Hash>) {

        }

        getSize() {
            const count = sumBy(this.list, (value:String | Int64 | UInt64 | PKAddress | Hash) => {
                if(typeof value === 'string') value = new Data.String(value)
                return value.getSize()
            })
            return count + getSizeOfVarInt(this.list.length) + 1
        }
        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(13,offset)
            offset = writeVarInt(this.list.length, buffer, offset)

            this.list.forEach((value:String | Int64 | UInt64 | PKAddress | Hash) =>{
                if(typeof value === 'string') value = new Data.String(value)
                offset = value.write(buffer, offset)
            })

            return offset
        }

        static read(buffer:Buffer, offset:number) : any {
            const {value:length, offset:offset2} = readVarInt(buffer, offset)
            offset = offset2

            var list = []

            for (var i = 0; i < length; i++) {
                var {data, offset:offset3} = read(buffer, offset)
                list.push(data)
                offset = offset3
            }

            return {data:new DataList(list), offset}
        }
    }

    export class Dictionary {
        constructor(public data:Array<[string, any]>) {

        }

        getSize() {
            const total = sumBy(this.data, pair => {
                if(Array.isArray(pair[1])) {
                    pair[1] = new DataList(pair[1])
                }
                return getSizeOfVarInt(pair[0].length) + pair[0].length + pair[1].getSize()
            })

            return total + getSizeOfVarInt(this.data.length) + 1
        }


        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(12, offset)
            offset = writeVarInt(this.data.length, buffer, offset)

            const pairs = this.data.sort( (x,y) => {
                const key1 = x[0]
                const key2 = y[0]

                if (key1.length < key2.length) {
                    return -1
                } else if (key1.length > key2.length) {
                    return 1
                } else {
                    for (let i = 0;i< key1.length; i++) {
                        const code1 = key1.charCodeAt(i)
                        const code2 = key2.charCodeAt(i)

                        if (code1 < code2)
                            return -1;
                        else if (code1 > code2)
                            return 1;
                    }

                    return 0
                }
            })

            pairs.forEach(pair => {
                offset = writeVarInt(pair[0].length,buffer,offset)
                offset += buffer.write(pair[0], offset, pair[0].length, 'ascii')
                offset = pair[1].write(buffer, offset)
            })

            return offset
        }

        static read(buffer:Buffer, offset:number) {
            const {value:length, offset:offset2} = readVarInt(buffer, offset)
            offset = offset2

            const data:Array<[string, any]> = []

            for (let i = 0; i < length; i++) {
                const {value: keyLength, offset:offset2} = readVarInt(buffer, offset)
                offset = offset2
                const key = buffer.toString('ascii', offset, offset + keyLength)
                offset += keyLength
                const {data:value, offset:offset3} = read(buffer, offset)
                offset = offset3

                data.push([key, value])
            }

            return {data:new Dictionary(data), offset}
        }
    }

    export class PKAddress {
        constructor(public hash:Buffer) {

        }

        getSize() {
            return 1 + getSizeOfVarInt(2) + getSizeOfVarInt(32) + 32
        }

        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(8,offset)
            offset = writeVarInt(2, buffer, offset)
            offset = writeVarInt(32, buffer,offset)
            this.hash.copy(buffer, offset)

            return offset + 32
        }

        static read(buffer:Buffer, offset:number) {
            const {value:identifier,offset:offset2} = readVarInt(buffer, offset)

            if (identifier !== 2)
                throw 'only pk lock is supported'

            const {value:size, offset:offset3} = readVarInt(buffer,offset2)

            if (size !== 32)
                throw 'invalid pk lock size'

            return {data:new PKAddress(buffer.slice(offset3, offset3 + 32)), offset:offset3 + 32}
        }
    }

    export class UInt64 {
        constructor(public value:BigInteger) {
        }

        getSize() {
            return 9
        }

        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(5,offset)

            this.value.toBuffer(8).copy(buffer, offset)

            return offset + 8
        }

        static read(buffer:Buffer, offset:number) {
            const value = BigInteger.fromBuffer(buffer.slice(offset, offset + 8))

            return {data:new UInt64(value), offset: offset + 8}
        }
    }

    export class Int64 {
        constructor(public value:BigInteger) {
        }

        getSize() {
            return 9
        }

        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(1,offset)
            this.value.toBuffer(8).copy(buffer, offset)

            return offset + 8
        }

        static read(buffer:Buffer, offset:number) {
            const value = BigInteger.fromBuffer(buffer.slice(offset, offset + 8))

            return {data:new Int64(value), offset: offset + 8}
        }
    }

    export class UInt32 {
        constructor(public value:BigInteger){

        }

        getSize() {
            return 5
        }
        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(4,offset)
            this.value.toBuffer(4).copy(buffer, offset)

            return offset + 4
        }

        static read(buffer:Buffer, offset:number) {
            const value = BigInteger.fromBuffer(buffer.slice(offset, offset + 4))

            return {data: new UInt32(value), offset: offset + 4}
        }
    }

    export class String {
        constructor(public s:string) {

        }

        getSize() {
            return this.s.length + 1 + getSizeOfVarInt(this.s.length)
        }

        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(6,offset)
            offset = writeVarInt(this.s.length, buffer, offset)
            buffer.write(this.s,offset,this.s.length, 'ascii')

            return offset + this.s.length
        }

        static read(buffer:Buffer, offset:number) {
            const {value:length, offset:offset2} = readVarInt(buffer, offset)

            const value = buffer.toString('ascii', offset2, offset2 + length)

            return {data: new String(value), offset: offset2 + length}
        }
    }

    export class Hash {
        constructor(public hash:TypesHash) {

        }

        getSize() {
            return this.hash.getSize() + 1
        }

        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(7,offset)
            return this.hash.write(buffer,offset)
        }

        static read(buffer:Buffer, offset: number) {
            const {hash, offset:offset2} = TypesHash.read(buffer, offset)

            return {data: new Hash(hash), offset: offset2}
        }
    }

    export class PublicKey {
        constructor(public pk:CryptoPublicKey) {

        }

        getSize() {
            return this.pk.serialize().length + 1
        }

        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(10,offset)

            return offset + this.pk.serialize().copy(buffer,offset)
        }

        static read(buffer:Buffer, offset:number) {
            const pk = CryptoPublicKey.deserialize(buffer.slice(offset, offset + 33))

            return {data:new PublicKey(pk), offset: offset + 33}
        }
    }
    
    export class Signature {
        constructor(public signature: CryptoSignature) {
            
        }
        
        getSize() {
            return this.signature.serialize().length + 1
        }

        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(9,offset)
            return offset + this.signature.serialize().copy(buffer,offset)
        }

        static read(buffer:Buffer, offset:number) {
            const signature = CryptoSignature.deserialize(buffer.slice(offset,offset + 64))

            return { data:new Signature(signature), offset: offset + 64}

        }
    }

    export function read(buffer:Buffer, offset:number) {
        const identifier = buffer.readInt8(offset)
        offset++

        switch (identifier) {
            case 1: return Int64.read(buffer, offset)
            //let private ByteData = 2uy
            //let private ByteArrayData = 3uy
            case 4: return UInt32.read(buffer, offset)
            case 5: return UInt64.read(buffer, offset)
            case 6: return String.read(buffer, offset)
            case 7: return Hash.read(buffer, offset)
            case 8: return PKAddress.read(buffer, offset)
            case 9: return Signature.read(buffer,offset)
            case 10: return PublicKey.read(buffer, offset)
            //let private CollectionArrayData = 11uy
            case 12: return Dictionary.read(buffer, offset)
            case 13: return DataList.read(buffer, offset)
            default:
                throw `data identifier: ${identifier} not yet implemented`
        }
    }

    function isDecCode(c:number) {
        return ((0x30/* 0 */ <= c) && (c <= 0x39/* 9 */));
    }

    function isNumber(data:string) {
        for (let index = 0; index < data.length; index++) {
            if (!isDecCode(data.charCodeAt(index))){
                return false
            }
        }

        return true
    }

    const DictionaryYamlType = new Type('tag:yaml.org,2002:map', {
        kind: 'mapping',
        construct: function (data) {
            return new Dictionary(data !== null ? data : {})
        },
        instanceOf: Dictionary,
        represent:function (data:any) {
            const dict:Dictionary=data
            return dict.data
        }
    })

    const StringYamlType = new Type('tag:yaml.org,2002:str', {
        kind: 'scalar',
        construct: function (data) {
            return new Data.String(data !== null ? data : '')
        },
        instanceOf:String,
        represent(data:any) {
            return data.s
        }
    })

    const Int64YamlType = new Type('tag:yaml.org,2002:int', {
        kind:'scalar',
        resolve(data) {
            if (typeof data === 'number') {
                return isInteger(data)
            }
            else if (typeof data === 'string') {
                if (data[0] === '-') {
                    data = data.substr(1)
                }

                return isNumber(data)
            }

            return false
        },
        construct: function (data) {
            if (typeof data === 'number') {
                return new Int64(new BigInteger(data.toString(),10,undefined))
            }
            else if (typeof data === 'string') {
                return new Int64(new BigInteger(data,10,undefined))
            }
        },
        instanceOf:Int64,
        represent: function (data:any) {
            const int64:Int64=data

            return int64.value.toString()
        }
    })

    const UInt64YamlType = new Type('!uint64', {
        kind:'scalar',
        resolve(data) {
            if (typeof data === 'string' && endsWith(data, 'UL')) {
                const amount = data.substr(0, data.length-2)

                return isNumber(amount)
            }

            return false
        },
        construct: function (data) {
            if (typeof data === 'string') {
                const amount = data.substr(0, data.length-2)

                return new UInt64(new BigInteger(amount, 10,undefined))
            }
        },
        instanceOf:UInt64,
        represent: function (data:any) {
            const uint64:UInt64=data

            return uint64.value.toString() + 'UL'
        }
    })

    const UInt32YamlType = new Type('!uint32', {
        kind:'scalar',
        resolve(data) {
            if (typeof data === 'string' && endsWith(data, 'ul')) {
                const amount = data.substr(0, data.length-2)

                return isNumber(amount)
            }

            return false
        },
        construct: function (data) {
            if (typeof data === 'string') {
                const amount = data.substr(0, data.length-2)

                return new UInt32(new BigInteger(amount, 10,undefined))
            }
        },
        instanceOf:UInt32,
        represent: function (data:any) {
            const uint32:UInt32=data

            return uint32.value.toString() + 'ul'
        }
    })

    const HashYamlType = new Type('!hash', {
        kind:'scalar',
        construct: function (data) {
            if (typeof data === 'string') {
                return new Hash(new TypesHash(data))
            }
        },
        instanceOf: Hash,
        represent: function(hash:any) {
            return hash.toString()
        }
    })

    const ZenAmountYamlType = new Type('!zen', {
        kind:'scalar',
        resolve: function (data) {
            if (typeof data === 'string' && endsWith(data, 'ZP')) {
                const amount = data.substr(0, data.length-2)

                if (isNumber(amount)) {
                    return true
                } else {
                    const decimalPoint = amount.indexOf('.')

                    if (decimalPoint === -1) {
                        return false
                    }

                    const x = amount.substr(0, decimalPoint)
                    const y = amount.substr(decimalPoint+1)

                    return isNumber(x) && isNumber(y)
                }
            }

            return false
        },

        construct: function (data) {
            // TODO:handle the case of number higher than javascript maximum

            const amount = data.substr(0, data.length-2).trim()

            const decimalPoint = amount.indexOf('.')

            let decimalLength = 0

            if (decimalPoint !== -1) {
                decimalLength = amount.length - decimalPoint - 1
            }

            let multiplier = Math.pow(10,8 - decimalLength)

            return new UInt64(new BigInteger(replace(amount, '.',''),10,undefined).multiply(BigInteger.valueOf(multiplier)))
        }
    })


    const PublicKeyYamlType = new Type('!pk', {
        kind:'scalar',
        construct:function(data) {
            const buffer = Buffer.from(data, 'hex')
            const publicKey = CryptoPublicKey.deserialize(buffer)

            return new PublicKey(publicKey)
        },
        instanceOf: PKAddress,
        represent: function(pk:any) {

            return pk.serialize().toString('hex')
        }
    })

    const basicSchema = new Schema({
        implicit:[ZenAmountYamlType,UInt64YamlType,Int64YamlType,UInt32YamlType],
        explicit:[
            StringYamlType,
            DictionaryYamlType,
            HashYamlType,
            PublicKeyYamlType
        ],
        include:[]
    })

    function createSchema(chain:Chain) {
        const AddressYamlType = new Type('!address', {
            kind:'scalar',
            resolve: function(data) {
                if (typeof data === 'string') {
                    try {
                        Address.decode(chain, data)
                        return true
                    }
                    catch (ex) {
                        return false
                    }
                }

                return false
            },
            construct:function(data) {
                const lock = Address.decode(chain, data)
                if (lock instanceof PKAddress) return lock
            },
            instanceOf: PKAddress,
            represent: function(pkLock:any) {

                return Address.getPublicKeyHashAddress(chain, pkLock.hash)
            }
        })

        return new Schema({
            include: [basicSchema],
            implicit: [AddressYamlType],
            explicit: []
        })
    }

    export function fromYaml(chain:Chain, yaml:string) {

        // console.log(createSchema(chain))

        const data = safeLoad(yaml, {schema: createSchema(chain)})

        // replacing object and strings
        function visit(node:any) : any {
            if (isPlainObject(node)) {

                const pairs = toPairs<any>(node).map<[string,String | Int64 | UInt64 | PKAddress | Hash]>(pair => {
                    const key:string = pair[0]
                    const value:String | Int64 | UInt64 | PKAddress | Hash = visit(pair[1])

                    return [key,value]
                })

                return new Dictionary(pairs)
            }
            else if (typeof node === 'string') {
                return new String(node)
            }

            return node
        }
        return visit(data)
    }

    export function toYaml(chain:Chain, data:any) {
        return safeDump(data, {schema:createSchema(chain)})
    }

    export function serialize(data:any) {
        const buffer = Buffer.alloc(data.getSize())
        data.write(buffer, 0)

        return buffer.toString('hex')
    }
}

export const fromYaml = Data.fromYaml
export const serialize = Data.serialize
export const toYaml = Data.toYaml
export const read = Data.read

export default Data
