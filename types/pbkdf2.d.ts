declare module 'pbkdf2' {
    class pbkdf2 {
        pbkdf2Sync(password:string, salt:Buffer, iterations:number,keyLength:number) : Buffer;
    }

    const instance:pbkdf2

    export = instance
}