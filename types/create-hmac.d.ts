declare module 'create-hmac' {
    class hmac {
        update(data:Uint8Array): void
        digest():Uint8Array
    }

    function createHmac(alg:string, secretKey:Uint8Array) : hmac;

    export = createHmac
}