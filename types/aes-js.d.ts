
declare module 'aes-js' {

    namespace aesjs {
        namespace ModeOfOperation {
            class cbc {
                constructor(key:Uint8Array,iv:Uint8Array)
                encrypt(plain:Uint8Array) : Uint8Array
                decrypt(cipher:Uint8Array) : Uint8Array
            }
        }
    }

    export = aesjs
}